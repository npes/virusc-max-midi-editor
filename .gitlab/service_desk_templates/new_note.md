New comment on your support issue number %{ISSUE_ID} 

Message:  

%{NOTE_TEXT}

Please respond to this email with follow up comments or questions if needed

[unsubscribe](https://gitlab.com/sent_notifications/incoming+npes-virusc-max-midi-editor-21401693-issue-@incoming.gitlab.com/unsubscribe)  

[unsubscribe](https://gitlab.com/npes/virusc-max-midi-editor-/sent_notifications/%{GitLab-Reply-Key}/unsubscribe)  


