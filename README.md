# VirusC Max Midi Editor

# Description

A controller for Access Virus C and Ableton live, made in max (for live) - including all the stuff that requires sending out sysex.  
Most of the sound editing functionality of the Virus C is implemented and tested.

- Entire mod matrix
- All Osc settings
- All filter settings
- Most Effect settings
- All LFO's settings
- All eq settings

Multi parts will not be implemented by me. Inputs, vocoder and patch naming/saving might be implemented at a later stage.

# Install instructions

1. Download https://gitlab.com/npes/virusc-max-midi-editor/-/raw/master/VirusC.amxd 
2. Copy it to the user library folder
3. Insert it on a midi channel
4. Set midi output of the channel to the one your Virus C is connected to

# Comments

This might work on different Virus versions but it is not tested (i only own a C model) if you test this please let me know.    

Using the virus as a remote controller midi device might not work, it creates a midi feedback with the current impementation which results in the amp envelope not working properly.  

Settings in the plugin will not autoupdate on program change, this is mainly due to Ableton not being able to handle bi-directional sysex see [https://help.ableton.com/hc/en-us/articles/360003148640-SysEx-support-in-Live-10](https://help.ableton.com/hc/en-us/articles/360003148640-SysEx-support-in-Live-10)  

It is a great tool to create new patches on the Virus and also for automating parameters from within live.  

# Collaboration and bugs

Collaboration and merge requests are welcome (especially if you are better at MAX than me)

Bugs can be reported by sending an email to: incoming+npes-virusc-max-midi-editor-21401693-issue-@incoming.gitlab.com  

# License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>